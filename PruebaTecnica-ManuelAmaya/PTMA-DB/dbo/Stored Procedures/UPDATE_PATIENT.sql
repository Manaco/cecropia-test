﻿CREATE PROCEDURE [dbo].[UPDATE_PATIENT] (
	@Id varchar(450),
	@blood int,
	@dateOfBirth datetime,
	@diseases varchar(100),
	@firstName varchar(50),
	@lastName varchar(50),
	@countryID int,
	@phone varchar(10)
) AS 
BEGIN

UPDATE [Patients] 
set BloodTypeId = @blood,
	DateOfBirth = @dateOfBirth,
	Diseases = @diseases,
	FirstName = @firstName,
	LastName = @lastName,
	NationalityId = @countryID,
	PhoneNumber = @phone
WHERE ID = @id
END 