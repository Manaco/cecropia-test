﻿CREATE TABLE [dbo].[Patients] (
    [Id]            NVARCHAR (450) NOT NULL,
    [BloodTypeId]   INT            NULL,
    [DateOfBirth]   DATETIME2 (7)  NOT NULL,
    [Diseases]      NVARCHAR (MAX) NULL,
    [FirstName]     NVARCHAR (MAX) NULL,
    [LastName]      NVARCHAR (MAX) NULL,
    [NationalityId] INT            NULL,
    [PhoneNumber]   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Patients] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Patients_BloodTypes_BloodTypeId] FOREIGN KEY ([BloodTypeId]) REFERENCES [dbo].[BloodTypes] ([Id]),
    CONSTRAINT [FK_Patients_Countries_NationalityId] FOREIGN KEY ([NationalityId]) REFERENCES [dbo].[Countries] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Patients_BloodTypeId]
    ON [dbo].[Patients]([BloodTypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Patients_NationalityId]
    ON [dbo].[Patients]([NationalityId] ASC);

