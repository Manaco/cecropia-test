﻿CREATE TABLE [dbo].[BloodTypes] (
    [Id]   INT            IDENTITY (1, 1) NOT NULL,
    [Type] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_BloodTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

