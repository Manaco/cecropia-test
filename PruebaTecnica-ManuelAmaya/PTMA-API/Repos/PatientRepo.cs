﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PTMA_API.Models;
using System.Threading.Tasks;

namespace PTMA_API.Repos
{
    // ===============================
    // AUTHOR : Manuel Amaya 
    // CREATE DATE : 01/31/2018
    // PURPOSE : Code Evaluation for Cecropia Solutions
    // Resolution of Internace for IPatient as Extra-layer to access data
    //  Stored procedure access Info:
    // https://dotnetthoughts.net/how-to-execute-storedprocedure-in-ef-core/
    // UpdateAsync Method is asynch in order to following the instructions, it uses a stored procedure too
    // ================================
    /// </summary>
    /// <seealso cref="PTMA_API.Repos.IPatientRepo" />
    public class PatientRepo : IPatientRepo
    {
        /// <summary>
        /// The context
        /// </summary>
        private ApiContext Context;

        /// <summary>
        /// The logger
        /// </summary>
        private ILogger Logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientRepo"/> class.
        /// </summary>
        /// <param name="_context">The context.</param>
        public PatientRepo(ApiContext _context, ILogger<PatientRepo> _looger)
        {
            Context = _context;
            Logger = _looger;
        }

        /// <summary>
        /// Creates the specified patient.
        /// </summary>
        /// <param name="patient">The patient.</param>
        /// <returns></returns>
        public string Create(Patient patient)
        {
            try
            {
                // validate model
                Context.Patients.Add(patient);
                Context.SaveChanges();
                return patient.Id;
            }
            catch (Exception ex)
            {
                Logger.LogError("Creation of patient problem", ex);
                return string.Empty;
            }        
            
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool Delete(string Id)
        {
            try
            {
                Context.Patients.Remove(Context.Patients.FirstOrDefault(x => x.Id.Equals(Id)));
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError("Deleting patient problem", ex);
                return false;
            }            
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public List<Patient> GetAll()
        {
            try
            {
                return Context.Patients.ToList();
            }
            catch (Exception ex)
            {
                Logger.LogError("Deletion of patient problem", ex);
                return new List<Patient>();
            }
            
        }

        /// <summary>
        /// Updates the specified patient.
        /// </summary>
        /// <param name="patient">The patient.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task<bool> UpdateAsync(Patient patient)
        {
            try
            {
                var result =  Context.Database.ExecuteSqlCommandAsync(@"UPDATE_PATIENT @Id,@blood,@dateOfBirth,@diseases,@firstName,@lastName,@countryID,@phone",
                 parameters: new[] { patient.Id,
                                    patient.BloodType.Id.ToString(),
                                    patient.DateOfBirth.ToString(),
                                    patient.Diseases,
                                    patient.FirstName,
                                    patient.LastName,
                                    patient.Nationality.Id.ToString(),
                                    patient.PhoneNumber});
                return result.Result != 0;
            }
            catch (Exception ex)
            {
                Logger.LogError("An error happened with the record was being updating", ex);
                return false;
            }
        }
    }
}
