﻿using Microsoft.EntityFrameworkCore;
using PTMA_API.Models;

namespace PTMA_API.Repos
{
    /// <summary>
    // ===============================
    // AUTHOR : Manuel Amaya 
    // CREATE DATE : 01/31/2018
    // PURPOSE : Code Evaluation for Cecropia Solutions
    // Patient Context and models definition in db
    // ===============================
    /// </summary>
    /// <seealso cref="Microsoft.EntityFrameworkCore.DbContext" />
    public class ApiContext:DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets the patients.
        /// </summary>
        /// <value>
        /// The patients.
        /// </value>
        public DbSet<Patient> Patients { get; set; }
        /// <summary>
        /// Gets or sets the countries.
        /// </summary>
        /// <value>
        /// The countries.
        /// </value>
        public DbSet<Country> Countries { get; set; }
        /// <summary>
        /// Gets or sets the blood types.
        /// </summary>
        /// <value>
        /// The blood types.
        /// </value>
        public DbSet<BloodType> BloodTypes { get; set; }
    }
}
