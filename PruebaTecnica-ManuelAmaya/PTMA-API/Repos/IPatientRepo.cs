﻿using PTMA_API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PTMA_API.Repos
{
    /// <summary>
    // ===============================
    // AUTHOR : Manuel Amaya 
    // CREATE DATE : 01/31/2018
    // PURPOSE : Code Evaluation for Cecropia Solutions
    // Interface to following the dependency injection style
    // ===============================
    /// </summary>
    public interface IPatientRepo
    {

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        List<Patient> GetAll();

        /// <summary>
        /// Creates the specified patient.
        /// </summary>
        /// <param name="patient">The patient.</param>
        /// <returns></returns>
        string Create(Patient patient);

        /// <summary>
        /// Updates the specified patient.
        /// </summary>
        /// <param name="patient">The patient.</param>
        /// <returns></returns>
        Task<bool> UpdateAsync(Patient patient);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns></returns>
        bool Delete(string Id);
    }
}
