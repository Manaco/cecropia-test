﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PTMA_API.Models;
using PTMA_API.Repos;

namespace PTMA_API.Controllers
{
    /// <summary>
    // ===============================
    // AUTHOR : Manuel Amaya 
    // CREATE DATE : 01/31/2018
    // PURPOSE : Code Evaluation for Cecropia Solutions
    // Patient Controller
    // Notes:  Reponses codes based in    
    // ===============================
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Route("api/[controller]")]
    public class PatientController : Controller
    {
        public IPatientRepo PatientRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientController"/> class.
        /// </summary>
        /// <param name="_patientRepo">The patient repo.</param>
        public PatientController(IPatientRepo _patientRepo)
        {
            PatientRepo = _patientRepo;
        }

        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {           
            var response = PatientRepo.GetAll();          

            if (response.Any())
                return new ObjectResult(response);

            return NotFound("No Patients to Show");
            
        }

        /// <summary>
        /// Adds the specified patient.
        /// </summary>
        /// <param name="patient">The patient.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] Patient patient)
        {
            var response = PatientRepo.Create(patient);
            if (string.IsNullOrEmpty(response))
                return BadRequest("A problem occurs when the new patiend triyng to be created");

            return CreatedAtRoute("Get", new { id = response }, patient);
        }

        /// <summary>
        /// Updates the specified patient.
        /// </summary>
        /// <param name="patient">The patient.</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] Patient patient)
        {
            var response = PatientRepo.UpdateAsync(patient);
            if (response.Result)
                return new NoContentResult();
            else
                return BadRequest($"A problem occurs when the patient {patient.Id} was being updated");
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            if (PatientRepo.Delete(id))
                return NoContent();
            else
                return BadRequest($"A problem happened deleting the Patient id {id}");
             
        }
    }
}