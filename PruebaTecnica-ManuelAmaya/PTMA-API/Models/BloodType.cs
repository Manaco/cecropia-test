﻿using System.Collections.Generic;

namespace PTMA_API.Models
{
    // ===============================
    // AUTHOR : Manuel Amaya 
    // CREATE DATE : 01/29/2018
    // PURPOSE : Code Evaluation for Cecropia Solutions
    // BloodType Model (Code First)
    // SPECIAL NOTES:
    // ===============================
    public class BloodType
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type { get; set; }
        /// <summary>
        /// Gets or sets the patients.
        /// </summary>
        /// <value>
        /// The patients.
        /// </value>
        public ICollection<Patient> Patients { get; set; }
    }
}