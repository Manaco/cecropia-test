﻿using System.Collections.Generic;

namespace PTMA_API.Models
{
    // ===============================
    // AUTHOR : Manuel Amaya 
    // CREATE DATE : 01/29/2018
    // PURPOSE : Code Evaluation for Cecropia Solutions
    // Country Model (Code First)
    // SPECIAL NOTES:
    // 
    // ===============================
    public class Country
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the name of the country.
        /// </summary>
        /// <value>
        /// The name of the country.
        /// </value>
        public string CountryName { get; set; }
        /// <summary>
        /// Gets or sets the patients.
        /// </summary>
        /// <value>
        /// The patients.
        /// </value>
        public ICollection<Patient> Patients { get; set; }
    }
}