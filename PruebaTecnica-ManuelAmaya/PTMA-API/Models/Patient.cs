﻿using System;

namespace PTMA_API.Models
{
    // ===============================
    // AUTHOR : Manuel Amaya 
    // CREATE DATE : 01/29/2018
    // PURPOSE : Code Evaluation for Cecropia Solutions
    // Patient (Code First)
    // SPECIAL NOTES:
    // This clase is used to generate the migrations too
    // ===============================
    public class Patient
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        public string FirstName { get; set; }
        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        public string LastName { get; set; }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Id { get; set; }
        /// <summary>
        /// Gets or sets the date of birth.
        /// </summary>
        /// <value>
        /// The date of birth.
        /// </value>
        public DateTime DateOfBirth { get; set; }
        /// <summary>
        /// Gets or sets the nationality.
        /// </summary>
        /// <value>
        /// The nationality.
        /// </value>
        public virtual Country Nationality { get; set; }
        /// <summary>
        /// Gets or sets the diseases.
        /// </summary>
        /// <value>
        /// The diseases.
        /// </value>
        public string Diseases { get; set; }
        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        /// <value>
        /// The phone number.
        /// </value>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Gets or sets the type of the blood.
        /// </summary>
        /// <value>
        /// The type of the blood.
        /// </value>
        public virtual BloodType BloodType { get; set; }
    }
}